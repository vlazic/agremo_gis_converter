#!/usr/bin/env python

"""Tests for `agremogis` package."""

import os
import pytest

from click.testing import CliRunner

from agremogis import Gis, REPORT_TYPES
from agremogis import cli


# abs path to file


def sample_path(file):
    return os.path.join(os.path.dirname(__file__), file)


@pytest.fixture
def instances():
    return {
        'LineString': {
            'GeoJSON': Gis(sample_path('sample_files/line_features.json')),
            'ShapeFile': Gis(sample_path('sample_files/line_features.zip'))
        },
        'Point': {
            'GeoJSON': Gis(sample_path('sample_files/points_feature.json')),
            'ShapeFile': Gis(sample_path('sample_files/point_features.zip'))
        },
        'Polygon': {
            'GeoJSON': Gis(sample_path('sample_files/poly_features.json')),
            'ShapeFile': Gis(sample_path('sample_files/poly_features.zip'))
        },
    }


def test_compare_reports(instances):
    for report_type in REPORT_TYPES:
        for geometry_type in instances.keys():
            # for geometry_type in ['Point', 'LineString']:
            gj = instances[geometry_type]['GeoJSON'].generate_report(
                report_type, False)
            sh = instances[geometry_type]['ShapeFile'].generate_report(
                report_type, False)

            assert gj == sh, '"{}" reports should be equal'.format(
                geometry_type)


def test_command_line_interface():
    """Test the CLI."""
    runner = CliRunner()

    result = runner.invoke(cli.main)
    assert result.exit_code == 2
    assert 'Missing option "--input-file"' in result.output

    help_result = runner.invoke(cli.main, ['--help'])
    assert help_result.exit_code == 0

    help_result = runner.invoke(cli.main, ['-i', ''])
    assert help_result.exit_code == 0
