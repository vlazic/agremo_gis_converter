import os
import json
import tempfile
from shapely.geometry import mapping, shape
from shapely.geometry.polygon import Polygon, LineString, Point

from agremogis import ALLOWED_GEOMETRY_TYPES
from .decoder_template import DecoderTemplate
from .helpers import raise_if_not_allowed


class GeoJSON(DecoderTemplate):
    FILE_EXTENSION = 'json'

    def __init__(self, input_file_path=None):
        if input_file_path is not None:
            try:
                with open(input_file_path) as file:
                    content = json.load(file)
            except BaseException as e:
                raise e
            else:
                self.parse(content)

    def parse(self, content):
        features = content['features']

        self.geometry_type = features[0]['geometry']['type']
        self.__geo_interface__ = {}
        self.__geo_interface__["features"] = []

        for feature in features:
            # for feature in features:
            if feature['geometry']['type'] == self.geometry_type:
                coordinates = feature['geometry']['coordinates']
                properties = feature['properties']

                if self.geometry_type == 'Point':
                    geometry = Point(*coordinates)
                elif self.geometry_type == 'LineString':
                    geometry = LineString(coordinates)
                elif self.geometry_type == 'Polygon':
                    geometry = Polygon(coordinates[0], coordinates[1:])

                self.__geo_interface__['features'].append({
                    "geometry": mapping(geometry),
                    "properties": properties
                })
        # print(len(self.__geo_interface__['features']))

    def export(self, data):
        data['type'] = 'FeatureCollection'

        for feature in data['features']:
            feature['type'] = 'Feature'

        file_path = self.export_file_path()

        with open(file_path, 'wb') as file:
            file.write(json.dumps(data, default=self.__to_json,
                                  ensure_ascii=False).encode('utf8'))

        return file_path

    # convert tuple to list during json conversion
    # https://stackoverflow.com/a/29909091
    def __to_json(self, python_object):
        if isinstance(python_object, tuple):
            python_object = {'__class__': 'tuple',
                             '__value__': list(python_object)}
