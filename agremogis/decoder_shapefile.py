import os
import json
import shapefile
import shutil
from glob import glob
from zipfile import ZipFile
from tempfile import TemporaryDirectory
from shapely.geometry import mapping, shape
from shapely.geometry.polygon import Polygon, LineString, Point

from .decoder_template import DecoderTemplate


class ShapeFile(DecoderTemplate):
    FILE_EXTENSION = 'zip'

    def __init__(self, input_file_path=None):
        if input_file_path is not None:
            with TemporaryDirectory() as temp_dir:
                try:
                    with ZipFile(input_file_path, 'r') as zip_ref:
                        # extract zip file to temporary directory
                        zip_ref.extractall(temp_dir)

                        # locate shapefile file
                        shp_file = glob(temp_dir + '/*.shp')[0]

                        # open shapefile file
                        content = shapefile.Reader(shp_file)

                except BaseException as e:
                    raise e
                else:
                    self.parse(content)

    def parse(self, content):
        self.__geo_interface__ = content.__geo_interface__
        self.geometry_type = self.__geo_interface__[
            'features'][0]['geometry']['type']

    def export(self, data):
        file_path = self.export_file_path(include_extension=False)

        with TemporaryDirectory() as tmp_dir_name:
            temp_files = os.path.join(tmp_dir_name, 'export')

            sh_writer = shapefile.Writer(temp_files)
            sh_writer.autoBalance = 1

            # create fields
            props = data['features'][0]['properties'].keys()
            for prop in props:
                sh_writer.field(str(prop), 'C', '50')

            for record in data['features']:
                # create records
                attrs = []
                for prop in props:
                    attrs.append(str(record['properties'][str(prop)]))
                sh_writer.record(*attrs)

                # create shapes
                sh_writer.shape(record['geometry'])

            sh_writer.close()

            # create zip archive
            full_file_path = shutil.make_archive(
                file_path, 'zip', root_dir=tmp_dir_name)

        return full_file_path
