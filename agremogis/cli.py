"""Console script for agremogis."""
import sys
import click
import agremogis
import shutil


@click.command()
@click.option('--input-file', '-i', required=True, type=str, help="Path to input file")
@click.option('--output-file', '-o', type=str, help="Path to output file")
@click.option('--report', '-r', type=click.Choice(agremogis.REPORT_TYPES, case_sensitive=False),
    default=agremogis.REPORT_TYPES[0], help="Generate report")
@click.option('--convert', '-c', type=click.Choice(agremogis.AVAILABLE_FORMATS, case_sensitive=False),
    default=agremogis.AVAILABLE_FORMATS[0], help="Convert to other format")
def main(input_file, output_file, report, convert):
    """Console script for agremogis."""

    if input_file:
        gis = agremogis.Gis(input_file)

        if report:
            tmp_output_file_path = gis.generate_report(report)
        elif convert:
            tmp_output_file_path = gis.export_to(convert)
        else:
            click.echo("Pick your action: report or convert. Use --help for full list of commands")
            return 1
        
        if output_file:
            shutil.move(tmp_output_file_path, output_file)
        else:
            with open(tmp_output_file_path,'r') as tmp_output_file:
                click.echo(tmp_output_file.read())

    return 0

if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
