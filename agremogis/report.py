import json
import pyproj
import shapely.ops as ops
from dicttoxml import dicttoxml
from tempfile import NamedTemporaryFile
from functools import partial
from shapely.geometry import mapping, shape
from shapely.geometry.polygon import Polygon, LineString, Point

from .helpers import raise_if_not_allowed

REPORT_TYPES = ['json', 'xml']


class Report:
    __report = {}

    def __init__(self, data, geometry_type):
        if geometry_type == 'Point':
            self.__pointReport(data['features'])
        elif geometry_type == 'LineString':
            self.__lineStringReport(data['features'])
        elif geometry_type == 'Polygon':
            self.__polygonReport(data['features'])

    def __pointReport(self, features):
        """For a file that contains Point geometries, the following info must be shown:
        a) Number of Points provided inside the file
        b) Extent (BoundaryBox) of all points.
        """

        # Number of Points provided inside the file
        self.__report['points'] = len(features)

        # Extent (BoundaryBox) of all points.
        minx, miny = maxx, maxy = features[0]['geometry']['coordinates']

        for feature in features:
            x, y = feature['geometry']['coordinates']
            minx, maxx = min(minx, x), max(maxx, x)
            miny, maxy = min(miny, y), max(maxy, y)

        self.__report['boundarybox'] = (minx, miny, maxx, maxy)

    def __lineStringReport(self, features):
        """For a file that contains LineString geometries, we want the following information to be shown:
        a) Number of LineStrings provided
        b) Length of longest linestring in meters
        c) Length of shortest linestring in meters
        """

        # Number of LineStrings provided
        self.__report['linestrings'] = len(features)

        shortestLineString = longestLineString = LineString(
            features[0]['geometry']['coordinates'])

        for feature in features[1:]:
            lineString = LineString(feature['geometry']['coordinates'])

            if shortestLineString.length > lineString.length:
                shortestLineString = lineString

            if longestLineString.length < lineString.length:
                longestLineString = lineString

        # Length of longest linestring in meters
        self.__report['shortestlinestring'] = self.__transformProjection(
            shortestLineString).length
        # Length of shortest linestring in meters
        self.__report['longestlinestring'] = self.__transformProjection(
            longestLineString).length

    def __polygonReport(self, features):
        """For polygons, as an output we are expecting to see the following information:
        a) Number of polygons provided
        b) Area of the biggest polygon in m​^2​
        c) Area of the smallest polygon in m​^2​
        """

        # Number of polygons provided
        self.__report['polygons'] = len(features)

        min_poly = max_poly = Polygon(
            features[0]['geometry']['coordinates'][0])

        for feature in features[1:]:
            poly = Polygon(feature['geometry']['coordinates'][0])

            if poly.area < min_poly.area:
                min_poly = poly

            if poly.area > max_poly.area:
                max_poly = poly

        # Area of the biggest polygon in m​^2​
        self.__report['minarea'] = self.__transformProjection(min_poly).area
        # Area of the smallest polygon in m​^2​
        self.__report['maxarea'] = self.__transformProjection(max_poly).area

    def __transformProjection(self, geometry):
        return ops.transform(
            partial(
                pyproj.transform,
                pyproj.Proj('EPSG:4326'),
                pyproj.Proj('EPSG:3395')),
            shape(geometry))

    def getReport(self, report_type='json', return_file=True):
        raise_if_not_allowed(report_type, REPORT_TYPES)

        if report_type == 'json':
            report_content = json.dumps(
                self.__report, ensure_ascii=False).encode('utf8')

        elif report_type == 'xml':
            report_content = dicttoxml(
                self.__report, custom_root='report')

        if return_file:
            tmp_file = NamedTemporaryFile(
                delete=False, suffix='.' + report_type, mode='wb')
            report_file = tmp_file.name

            try:
                tmp_file.write(report_content)
            finally:
                tmp_file.close()

            return report_file

        else:
            return report_content
