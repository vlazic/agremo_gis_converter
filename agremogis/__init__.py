"""Top-level package for agremogis."""

__author__ = """Vladimir Lazic"""
__email__ = 'vlazic@gmail.com'
__version__ = '0.1.0'


from pkgutil import extend_path, walk_packages

from os.path import isfile, abspath

# report class
from .report import Report, REPORT_TYPES

# GIS file decoder classes
# from .file_template import FileTemplate
# from .geojson import GeoJson

# helper functions
from .helpers import raise_if_not_allowed

ALLOWED_GEOMETRY_TYPES = ['Point', 'LineString', 'Polygon']

# dynamically import all decoder modules
# https://michaelheap.com/python-dynamically-load-all-modules-in-a-folder/
__path__ = extend_path(__path__, __name__)
for _, modname, __ in walk_packages(path=__path__):
    if modname.startswith('decoder_'):
        __import__(__name__ + '.' + modname)

# Collect format, extensions and class refference
# of all available suported GIS file parsers
# {
#     'GeoJSON':
#         ('GeoJSON', 'json', <class 'agremogis.geojson.GeoJson'>)
# }
PARSERS = {
    f.__name__: (f.__name__, f.FILE_EXTENSION, f)
    for f in decoder_template.DecoderTemplate.__subclasses__()  # pylint: disable=undefined-variable
}

# GeoJSON, ShapeFile etc.
AVAILABLE_FORMATS = list(PARSERS.keys())

# json, zip etc.
ALLOWED_EXTENSIONS = [ext[1] for ext in PARSERS.values()]


class Gis:
    __geo_interface__ = None
    __geometry_type = None
    __file_format = None
    __file_path = None

    def __init__(self, input_file_path, input_file_format=None):
        if not isfile(input_file_path):
            raise IOError("File {} does't exist".format(input_file_path))

        self.__file_path = abspath(input_file_path)

        # get input file extension
        input_file_extension = self.__file_path.split(".")[-1]

        for file_format, file_extension, DecoderClass in PARSERS.values():
            # print(input_file_extension, file_extension)
            # print(file_format, input_file_format)

            # determine proper decoding format for file
            if file_format == input_file_format \
                    or file_extension == input_file_extension:

                # create instance of decoder class
                decoder = DecoderClass(self.__file_path)

                # get decoded data and geometry type
                self.__geo_interface__ = decoder.__geo_interface__
                self.__geometry_type = decoder.geometry_type
                # save current file format
                self.__file_format = file_format

                # exit the loop, we already found correct file format
                break

        if self.__file_format == None:
            raise ValueError("Allowed formats: {}. allowed extensions: {}".format(
                ', '.join(AVAILABLE_FORMATS), ', '.join(ALLOWED_EXTENSIONS))
            )

    def generate_report(self, report_type='json', return_file=True):
        raise_if_not_allowed(report_type, REPORT_TYPES)

        if self.__geo_interface__ is None:
            raise ValueError('GIS data is not successfully parsed')

        report = Report(self.__geo_interface__, self.__geometry_type)

        return report.getReport(report_type, return_file)

    def export_to(self, export_format=None):
        raise_if_not_allowed(export_format, AVAILABLE_FORMATS)

        if self.__geo_interface__ is None:
            raise ValueError('GIS data is not successfully parsed')

        # create instance of parser class we want to convert to
        parser_instance = PARSERS[export_format][2]()

        # run the 'export' function
        exported_data = parser_instance.export(self.__geo_interface__)

        return exported_data


# if __name__ == '__main__':
#     import sys
#     gis = Gis(sys.stdin)
