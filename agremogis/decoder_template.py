import os
import tempfile
from datetime import datetime

from abc import ABC, abstractmethod
from . import ALLOWED_GEOMETRY_TYPES


class DecoderTemplate(ABC):
    __geometry_type = None
    __geo_interface__ = None

    def export_file_path(self, include_extension=True):
        time_str = datetime.now().strftime("%d-%m-%Y_%I-%M-%S_%p")

        if include_extension:
            file_name = 'export_{}.{}'.format(time_str, self.FILE_EXTENSION)
        else:
            file_name = 'export_{}'.format(time_str)

        return os.path.join(tempfile.gettempdir(), file_name)

    @abstractmethod
    def parse(self, content):
        pass

    @abstractmethod
    def export(self):
        pass

    @property
    def geometry_type(self):
        return self.__geometry_type

    @geometry_type.setter
    def geometry_type(self, geometry_type):
        if geometry_type not in ALLOWED_GEOMETRY_TYPES:
            raise ValueError('{geometry_type} type is wrong, use {allowedTypes}'.format(
                geometry_type=repr(geometry_type),
                allowedTypes=', '.join([repr(i) for i in ALLOWED_GEOMETRY_TYPES]))
            )
        self.__geometry_type = geometry_type
