def raise_if_not_allowed(value_provided, allowed_values):
    """Raise ValueError if provided value is not allowed
    """
    if value_provided not in allowed_values:
        raise ValueError('{value_provided} is wrong, use {allowed_values}'.format(
            value_provided=repr(value_provided),
            allowed_values=', '.join([repr(i) for i in allowed_values]))
        )
